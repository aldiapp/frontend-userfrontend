import { Routes, Route, BrowserRouter} from 'react-router-dom';
import ShowServices from './components/ShowServices';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<ShowServices></ShowServices>}></Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App;
